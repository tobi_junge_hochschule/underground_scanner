#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()
left_motor = Motor(Port.A)
right_motor = Motor(Port.D)
light_sensor = ColorSensor(Port.S1)
wheel_dimensions = 56
axle_track = 120
drive_base = DriveBase(left_motor, right_motor, wheel_dimensions, axle_track)



values = []
# Write your program here.
drive_base.drive(10, 0)
while drive_base.distance() < 120:
    values.append(light_sensor.reflection())
drive_base.stop()
print(values)
